import d3 from 'd3';
import angular from 'angular';
import cloud from 'd3-cloud';
import { each } from 'underscore';

import editorTemplate from './word-cloud-editor.html';

function findWordFrequencies(data, columnName, frequencyColumn = null) {
  const wordsHash = {};
  let maxFrequency = 1;
  data.forEach((row) => {
    const wordsList = [row[columnName].toString()];
    let wordFrequency = 1;
    if (frequencyColumn != null) {
      wordFrequency = parseInt(row[frequencyColumn], 10);
    }

    wordsList.forEach((d) => {
      if (d in wordsHash) {
        wordsHash[d] += wordFrequency;
      } else {
        wordsHash[d] = wordFrequency;
      }
      if (wordsHash[d] > maxFrequency) {
        maxFrequency = wordsHash[d];
      }
    });
  });
  return [wordsHash, maxFrequency];
}


function wordCloudRenderer() {
  return {
    restrict: 'E',
    link($scope, elem) {
      function reloadCloud() {
        if (!angular.isDefined($scope.queryResult)) return;

        const data = $scope.queryResult.getData();
        let wordsHash = {};
        let maxFrequency = 1;

        if ($scope.visualization.options.column) {
          const newColumn = $scope.visualization.options.frequency_column;
          const responseObject = findWordFrequencies(data, $scope.visualization.options.column, newColumn);
          wordsHash = responseObject[0];
          maxFrequency = responseObject[1];
        }

        let sizeDivider = 1;
        if ($scope.visualization.options.sizeDivider) {
          sizeDivider = parseInt($scope.visualization.options.sizeDivider, 10);
        }
        let canvasSize = 500;
        if ($scope.visualization.options.canvasSize) {
          canvasSize = parseInt($scope.visualization.options.canvasSize, 10);
        }


        const wordList = [];
        each(wordsHash, (v, key) => {
          const dividedValue = (v / sizeDivider);
          const p = (v * 100) / maxFrequency;
          wordList.push({
            text: key, size: 10 + (Math.pow(dividedValue, 2)), value: v, percentile: parseInt(p, 10)
          });
        });

        const fill = d3.scale.category20();
        const layout = cloud()
          .size([canvasSize, canvasSize])
          .words(wordList)
          .padding(5)
          .rotate(() => Math.floor(Math.random() * 2) * 90)
          .font('Impact')
          .fontSize(d => d.size);

        function draw(words) {
          d3.select(elem[0].parentNode)
            .select('svg')
            .remove();

          d3.select(elem[0].parentNode)
            .append('svg')
            .attr('width', layout.size()[0])
            .attr('height', layout.size()[1])
            .append('g')
            .attr('transform', `translate(${layout.size()[0] / 2},${layout.size()[1] / 2})`)
            .selectAll('text')
            .data(words)
            .enter()
            .append('text')
            .style('font-size', d => `${d.size}px`)
            .style('font-family', 'Impact')
            .style('fill', (d, i) => fill(i))
            .attr('text-anchor', 'middle')
            .attr('transform', d =>
              `translate(${[d.x, d.y]})rotate(${d.rotate})`)
            .text(d => d.text)
            .append('svg:title')
              .text(function(d) {
                return d.text+'\nfrequency: '+d.value+'\npercentile: '+d.percentile;
              });
        }
        layout.on('end', draw);

        layout.start();

      }

      $scope.$watch('queryResult && queryResult.getData()', reloadCloud);
      $scope.$watch('visualization.options.column', reloadCloud);
      $scope.$watch('visualization.options.frequency_column', reloadCloud);
      $scope.$watch('visualization.options.sizeDivider', reloadCloud);
      $scope.$watch('visualization.options.canvasSize', reloadCloud);
    },
  };
}

function wordCloudEditor() {
  return {
    restrict: 'E',
    template: editorTemplate,
  };
}


export default function init(ngModule) {
  ngModule.directive('wordCloudEditor', wordCloudEditor);
  ngModule.directive('wordCloudRenderer', wordCloudRenderer);

  ngModule.config((VisualizationProvider) => {
    VisualizationProvider.registerVisualization({
      type: 'WORD_CLOUD',
      name: 'Word Cloud',
      renderTemplate: '<word-cloud-renderer options="visualization.options" query-result="queryResult"></word-cloud-renderer>',
      editorTemplate: '<word-cloud-editor></word-cloud-editor>',
    });
  });
}
